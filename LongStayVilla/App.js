/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import { StyleSheet, Text, View, Image, Button,TouchableOpacity} from 'react-native';
 import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Homepage from './src/components/Homepage';
import Login from './src/components/Login';
import BookVilla from './src/components/BookVilla';
import HomeTab from './src/components/HomeTab';
import LocationTab from './src/components/LocationTab';
import BookingsTab from './src/components/BookingsTab';
import ProfileTab from './src/components/ProfileTab';
import MoreTab from './src/components/MoreTab';
import NewBooking from './src/components/NewBooking';
import BookingConfirm from './src/components/BookingConfirm';
import { createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

//  var express = require('express')
//  var cors = require('cors')
 
//  var app = express()
//  app.use(cors()); //cors allows 
//  app.use(express.json());
//  app.use(express.urlencoded({ extended: false }));
 
//  {id, name, category, provider, origin, units} 
 
//  var data =[
//   {"id": "1", "image":"http://training.pyther.com/long-stay-villa/001_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "2", "image":"http://training.pyther.com/long-stay-villa/003_villa.jpg","title": "An ultra-exclusive mountain home Ranch-inspired luxury living","price":"$3500","Location":"Aspen"},
//   {"id": "3", "image":"http://training.pyther.com/long-stay-villa/009_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "4", "image":"http://training.pyther.com/long-stay-villa/002_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "5", "image":"http://training.pyther.com/long-stay-villa/004_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "6", "image":"http://training.pyther.com/long-stay-villa/005_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "7", "image":"http://training.pyther.com/long-stay-villa/006_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "8", "image":"http://training.pyther.com/long-stay-villa/007_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "9", "image":"http://training.pyther.com/long-stay-villa/008_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "10", "image":"http://training.pyther.com/long-stay-villa/010_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},

    //  {"id":"2", "title":"Spanish for Beginners", "author":"Taks", "publisher":"Mara House", "isbn":"73fs4222", "year":1978, "cover":"http://training.pyther.com/long-stay-villa/003_villa.jpg"},
    //  {"id":"3", "title":"Fat Cat on Mat", "author":"Mat", "publisher":"Indus House", "isbn":"746fs4222", "year":1999, "cover":"http://training.pyther.com/books/9781409509233_cover_image.jpg"},
    //  {"id":"4","title":"Write Your Own Story Book", "author":"Manish", "publisher":"Indus Books", "isbn":"76fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781409523352_cover_image.jpg"},
    //  {"id":"5","title":"100 Papers Dragan Fold and Fly", "author":"Vivek", "publisher":"Indus House", "isbn":"746fs4222", "year":2010, "cover":"http://training.pyther.com/books/9781409598596_cover_image.jpg"},
    //  {"id":"6","title":"FingerPrint Activities", "author":"Tulsidas", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474914338_cover_image.jpg"},
    //  {"id":"7", "title":"First Illustrated English", "author":"Taks", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474941044_cover_image.jpg"},
    //  {"id":"8", "title":"Biggest Tallest Fastest", "author":"Tulsidas", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474950589_cover_image.jpg"},
    //  {"id":"9", "title":"Book of the Brain", "author":"Tulsidas", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474950855_cover_image.jpg"},
    //  {"id":"10", "title":"Looking After our Planet", "author":"Tulsidas", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474959940_cover_image.jpg"},
    //  {"id":"11", "title":"The Shark Caller", "author":"Taks", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474966849_cover_image.jpg"},
    //  {"id":"12","title":"Looking After Our Planet", "author":"Smith", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474968942_cover_image.jpg"},
    //  {"id":"13", "title":"The Sleeping Prince", "author":"Smith", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474969802_cover_image.jpg"},
    //  {"id":"14", "title":"The UnHappy Book", "author":"Taks", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474970495_cover_image.jpg"},
    //  {"id":"15", "title":"The Infinity Files", "author":"Tulsidas", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474972208_cover_image.jpg"},
    //  {"id":"16", "title":"24 hours in the Stone Age", "author":"Smith", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474977111_cover_image.jpg"},
    //  {"id":"17","title":"Billy Mini and The Monsters", "author":"Taks", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474973439_cover_image.jpg"},
    //  {"id":"18","title":"Don't Tickle the Pig", "author":"Tulsidas", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474981323_cover_image.jpg"},
    //  {"id":"19","title":"Poppy Sam and Lamb", "author":"Vivek", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474981354_cover_image.jpg"},
    //  {"id":"20","title":"First Coloring Springtime", "author":"Taks", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474985420_cover_image.jpg"},
    //  {"id":"21","title":"Ready For Writing", "author":"Smith", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474986694_cover_image.jpg"},
    //  {"id":"22","title":"What is a Virus", "author":"Smith", "publisher":"Indus House", "isbn":"746fs4222", "year":1983, "cover":"http://training.pyther.com/books/9781474991513_cover_image.jpg"}
//  ];

//  app.get('/api/long-stay', function (req, res) {
//   res.send(data);
//});

const Stack = createStackNavigator();
const App = () => {
     return (
      
        <NavigationContainer>
        <Stack.Navigator initialRouteName="Homepage">
          <Stack.Screen name="Homepage" component = {Homepage} />
          <Stack.Screen name="Login" component = {Login} />
          <Stack.Screen name="BookVilla" component = {BookVilla} />
          <Stack.Screen name="NewBooking" component = {NewBooking} />
          <Stack.Screen name="BookingConfirm" component = {BookingConfirm} />
          {/* <Stack.Screen name="HomeTab" component = {HomeTab} />
          <Stack.Screen name="LocationTab" component = {LocationTab} />
          <Stack.Screen name="BookingsTab" component = {BookingsTab} />
          <Stack.Screen name="ProfileTab" component = {ProfileTab} />
          <Stack.Screen name="MoreTab" component = {MoreTab} /> */}
        </Stack.Navigator>
      </NavigationContainer>
      
     );
}
//app.listen(3000);
 export default App;