import { ContactSupportOutlined } from "@material-ui/icons";

export const fetchDetails = (id) => {
   // const id  = route.params;
  // console.log("fetchDetails "+id);
 //   const id = props.match.params.id;
    const apiURL = "https://nodeapi.pyther.com/api/long-stay/"+id;
  // console.log('hi',apiURL);
    return fetch(apiURL, {
      method: "GET",
      headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      }
    }).then((res)=> res.json())
    .then((result)=>{
        return result
        
    },(error)=>{
        console.log(">>>err",error)
        return error;
    })
  };
