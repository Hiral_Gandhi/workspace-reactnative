// var express = require('express')
//  var cors = require('cors')
 
//  var api = express()
//  api.use(cors()); //cors allows 
//  api.use(express.json());
//  api.use(express.urlencoded({ extended: false }));
 
//  //{id, name, category, provider, origin, units} 
 
//  var data =[
//   {"id": "1", "image":"http://training.pyther.com/long-stay-villa/001_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "2", "image":"http://training.pyther.com/long-stay-villa/003_villa.jpg","title": "An ultra-exclusive mountain home Ranch-inspired luxury living","price":"$3500","Location":"Aspen"},
//   {"id": "3", "image":"http://training.pyther.com/long-stay-villa/009_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "4", "image":"http://training.pyther.com/long-stay-villa/002_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "5", "image":"http://training.pyther.com/long-stay-villa/004_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "6", "image":"http://training.pyther.com/long-stay-villa/005_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "7", "image":"http://training.pyther.com/long-stay-villa/006_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "8", "image":"http://training.pyther.com/long-stay-villa/007_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "9", "image":"http://training.pyther.com/long-stay-villa/008_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},
//   {"id": "10", "image":"http://training.pyther.com/long-stay-villa/010_villa.jpg","title": "Villa Jane will astonish every guest with its breathtaking views","price":"$2000","Location":"Aspen"},

// ];

// api.get('/api/long-stay', function (req, res) {
//   res.send(data);
// });
// api.listen(3000);
export const fetchData = () => {
    
    const apiURL = "https://nodeapi.pyther.com/api/long-stay";
    return fetch(apiURL, {
      method: "GET",
      headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      }
    }).then((res)=> res.json())
    .then((result)=>{
        return result
    },(error)=>{
        console.log(">>>err",error)
        return error;
    })
  };
