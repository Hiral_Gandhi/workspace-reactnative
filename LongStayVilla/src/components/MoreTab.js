import React from 'react';
import { StyleSheet, Text, View,FlatList, Image, Button,TouchableOpacity, ImageBackground, Dimensions, ScrollView} from 'react-native';
import { Item } from 'react-native-paper/lib/typescript/components/List/List';
//import paymentIcon from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/AntDesign';
var screenHeight = Dimensions.get("screen").height;
import Login from './Login';
const data = [
  { id: '1',image: require('../assets/AddressIcon.png'),title: 'Booking Address',text:'Home',name:'rightcircle'},
  { id: '2',image: require('../assets/PaymentIcon.png'),title: 'Payment Method',text:'Home',name:'rightcircle'},
  { id: '3',image: require('../assets/CurrencyIcon.png'),title: 'Currency',text:'Home',name:'rightcircle'},
  { id: '4',image: require('../assets/LanguageIcon.png'),title: 'Language',text:'Home',name:'rightcircle'}
];

const MoreTab = ({navigation}) => {
    return (
      <ScrollView style={height="90%", backgroundColor="white"}>
      <View>  
      <View style={styles.container}>
      {/* <Text style={styles.text}>Basic FlatList Example</Text> */}
      
          
            {/* <Text style={styles.text}>{item.title}</Text> */}
            <View style={styles.homebutton}>
            <TouchableOpacity style={styles.addBtn} onPress= {() => navigation.navigate('Bookings')}>
            <View style={styles.homebutton}>
            <Image style={styles.btn} source={require('../assets/AddressIcon.png')}/>
            <Text style={{width:260,fontSize:18,color:"gray",marginLeft:20}}>{'Booking Address'}</Text>
            {/* <View> */}
                <View style={{alignItems:'flex-end'}}>
                <Icon name={'rightcircle'} style={{color:"#cccccc"}} size={20} />
                </View>
            </View>
            </TouchableOpacity>
          
            </View>  
         

         
            {/* <Text style={styles.text}>{item.title}</Text> */}
            <View style={styles.homebutton}>
            <TouchableOpacity style={styles.addBtn} onPress= {() => navigation.navigate('Bookings')}>
            <View style={styles.homebutton}>
            <Image style={styles.btn} source={require('../assets/PaymentIcon.png')}/>
            <Text style={{width:260,fontSize:18,color:"gray",marginLeft:20}}>{'Payment Method'}</Text>
            {/* <View> */}
                <View style={{alignItems:'flex-end'}}>
                <Icon name={'rightcircle'} style={{color:"#cccccc"}} size={20} />
                </View>
            </View>
            </TouchableOpacity>
          
            </View>  
          

        
            {/* <Text style={styles.text}>{item.title}</Text> */}
            <View style={styles.homebutton}>
            <TouchableOpacity style={styles.addBtn} onPress= {() => navigation.navigate('Bookings')}>
            <View style={styles.homebutton}>
            <Image style={styles.btn} source={require('../assets/CurrencyIcon.png')}/>
            <Text style={{width:260,fontSize:18,color:"gray",marginLeft:20}}>{'Currency'}</Text>
            {/* <View> */}
                <View style={{alignItems:'flex-end'}}>
                <Icon name={'rightcircle'} style={{color:"#cccccc"}} size={20} />
                </View>
            </View>
            </TouchableOpacity>
          
            </View>  
          

         
            {/* <Text style={styles.text}>{item.title}</Text> */}
            <View style={styles.homebutton}>
            <TouchableOpacity style={styles.addBtn} onPress= {() => navigation.navigate('Bookings')}>
            <View style={styles.homebutton}>
            <Image style={styles.btn} source={require('../assets/LanguageIcon.png')}/>
            <Text style={{width:260,fontSize:18,color:"gray",marginLeft:20}}>{'Language'}</Text>
            {/* <View> */}
                <View style={{alignItems:'flex-end'}}>
                <Icon name={'rightcircle'} style={{color:"#cccccc"}} size={20} />
                </View>
            </View>
            </TouchableOpacity>
          
            </View>  
         
     
      </View>
      <View style={styles.containernew}>
            <TouchableOpacity style={styles.logoutBtn} onPress={() => navigation.navigate('Login')}>
            <Text style={styles.btnText}>LOG OUT</Text>
            </TouchableOpacity> 
      </View>
    </View> 
    </ScrollView>
    );
};
const styles = StyleSheet.create({
  container: {  
    height: 230,
    width: "95%",
    borderRadius: 13,
    backgroundColor: "white",
    justifyContent: "space-around",
    marginTop: "30%",
    marginLeft:10
     },
  containernew:{
    marginTop:50,
   // flex:2,
  },
  addBtn: {    
      flex:1,
      backgroundColor: "white",
      },
  // paymentBtn: {    
  //     flex:1,
  //     backgroundColor: "white",
  //     },
  // currencyBtn: {    
  //     flex:1,
  //     backgroundColor: "white",
  //     },
  // languageBtn: {    
  //     flex:1,
  //     backgroundColor: "white",
  //     },
  listItem: {
          marginTop: 10,
        //  flex: 1,
          height:screenHeight/15,
          padding: 6,
          borderBottomColor:"#d3d3d3",
          borderBottomWidth:1,
          marginLeft:15,
          
         // alignItems: 'center',
        //  borderColor:"black",
        //  borderWidth:1,
         // fontSize:100,
          backgroundColor: '#fff',
          width: '100%'
        },
    homebutton:{
          flexDirection: "row",
        },
    text: {
          fontSize: 18,
          color: 'grey',
          justifyContent: "flex-start",
          paddingBottom: 10,
          marginLeft:30
        //  fontWeight: '700'
        
        },
        btn2:{
          width: 10,
          height: 10,
          //color: '#d3d3d3',
         // tintColor:'grey',
          
          marginTop:5,
          marginLeft:5
        },
    logoutBtn: {
          marginLeft:30,
          
          width: "85%",
      //    borderRadius: 25,
          height: 50,
          alignItems: "center",
          justifyContent: "center",
   //       marginTop: 50,
      //    backgroundColor: "#B2002D",
        },
    
        btnText:{
          color:"#B2002D",
         // fontWeight: 'bold',
        },

        btn:{
          width: 24,
          height: 22,
          //color: '#d3d3d3',
          tintColor:'grey',
          marginLeft:10
        },
        // homebutton:{
        //   flexDirection: "row",
       // },
        arrowbutton:{
          // flexDirection:"column",
        //  marginLeft:120
        width:20,
        height:20,
       // paddingLeft:10,
         backgroundColor: '#d3d3d3',
         // padding:10,
           borderRadius:360,
          // borderWidth:1
         },
    });
export default MoreTab;