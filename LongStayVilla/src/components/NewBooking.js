import React ,{ useEffect, useState }from 'react';
import { StyleSheet, Text, View, FlatList,Alert, ActivityIndicator, Image, Button,TouchableOpacity, ImageBackground, Dimensions, ScrollView} from 'react-native';
import {fetchDetails} from '../service/apidetails';
//import NewBooking from './src/components/NewBooking';
var screenHeight = Dimensions.get("screen").height;
var screenWidth = Dimensions.get("screen").width;
import Icon from 'react-native-vector-icons/AntDesign';


export default NewBooking = ({ route, navigation }) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);


  useEffect(() => {
    if(route.params.id){
      fetchDetails(route.params.id).then((result)=>{
        console.log("result",result)
        setData(result)
        setLoading(false)
    })
    }
    
  },[])

  const button =({}) =>{
   
    Alert.alert(
      '',
      'Are you sure you want to Book Villa?',
      [
        {text: 'CANCEL', onPress: () => console.warn('Cancelled'), style: 'cancel'},
        {text: 'YES', onPress: () => navigation.navigate('BookingConfirm')},
      ]
    );
  }
  
  return (
    <ScrollView>
        {/* {console.log('item'+isLoading)}    */}
      {isLoading ? <ActivityIndicator/> : (
          <View>
            {/* {console.log('item',item)} */}
            <View style={styles.container2}>
                  <Text style={styles.btnText2}>Price {data.price}</Text>
            </View>
            <Image style={styles.listItem} source={{uri:data.image}}/>
            <View style={styles.newbutton}>
                  <Text style={styles.btnText}>Amiantus</Text>
                  <Text style={styles.btnText2}>Details</Text>
                  <Text style={styles.btnText3}>Reviews</Text>
            </View>
            <View style={styles.newbutton2}>
                  <Text style={styles.btnText5}>BADROOM</Text>
                  <Text style={styles.btnText4}>{data.rooms}</Text>
            </View>
            <View style={styles.newbutton3}>
                  <Text style={styles.btnText5}>TOTAL AREA</Text>
                  <Text style={styles.btnText6}>{data.totalarea}</Text>
            </View>
            <View style={styles.containernew}>
                  <Text style={styles.textnew}>{data.title}</Text>
            </View>
                     <View style={styles.homebutton}>
                    <TouchableOpacity style={styles.moreBtn} onPress={() => button('BookingConfirm')}>
                    <View style={styles.homebutton}>
                        <Text style={styles.btnTextnew}>Book</Text>
                        <Icon name={'rightcircle'} style={styles.icon} size={33} />
                    </View>   
                    </TouchableOpacity> 
                    </View>                   
          </View>   
      )} 
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
   // alignItems: 'center'
  },
  container2: {
    marginTop:20,
    marginLeft:190,
    justifyContent: "center",
    alignItems: 'center',
  },
  containernew: {
    //   marginTop: 30,
     //  backgroundColor: "white",
    // flex:1,
     alignItems: "center",
     justifyContent: "center",
     paddingLeft:10,
     paddingRight:10
     },
  text: {
    fontSize: 18,
    color: '#101010',
    justifyContent: "flex-start",
    padding: 5,
  //  fontWeight: '700'
  
  },
  homebutton:{
    flexDirection: "row",
  },
  textnew:{
    fontSize:24,
    color:"#696969",
    marginTop:40, 
    textAlign:"center",
   // marginLeft:55,
  
},
  price: {
    fontSize: 18,
    color: '#B2002D',
    justifyContent: "flex-start",
    marginLeft:5,
  //  padding: 5,
  //  fontWeight: '700'
  
  },
  listItem: {
    marginTop: 20,
    //marginLeft:80,
   // flex: 1,
     height:screenHeight/3.5,
    left: 5,
   // right:10,
   justifyContent: "center",
    alignItems: 'center',
  //  borderColor:"black",
  //  borderWidth:1,
   // backgroundColor: '#fff',
    width: '98%'
  },
  listItemText: {
    fontSize: 18
  },
  bookvillaBtn: {
   // flexDirection: "row",
   marginLeft:10,
    width: "31%",
    borderRadius: 25,
    height: 27,
    alignItems: "center",
    justifyContent: "center",
 //   marginTop: 5,
    backgroundColor: "#B2002D",
    marginBottom:10,
  },
  moreBtn: {
  //  flexDirection: "row",
   // marginLeft:195,
    marginTop:45,
    width: screenWidth/2,
    borderRadius: 30,
    height: 55,
    alignItems: "center",
    justifyContent: "center",
 //   marginTop: 15,
    backgroundColor: "#B2002D",
 //  marginBottom:10,
  },
  btnText:{
    color:"grey",
   // fontWeight: 'bold',
    fontSize: 18,
    marginBottom: '5%',
    marginLeft:10
    
  },
  btnText2:{
    color:"#B2002D",
   // fontWeight: 'bold',
    fontSize: 18,
    marginBottom: '5%',
    marginLeft:85,
    
  },
  btnText3:{
    color:"grey",
   // fontWeight: 'bold',
    fontSize: 18,
    marginBottom: '5%',
    marginLeft:85
  },
  btnText4:{
    color:"grey",
   // fontWeight: 'bold',
    fontSize: 18,
    marginBottom: '5%',
    marginLeft:260,
    left:10
  },
  btnText5:{
    color:"grey",
   // fontWeight: 'bold',
    fontSize: 14,
    marginBottom: '5%',
   // marginLeft:10
    
  },
  btnText6:{
    color:"grey",
   // fontWeight: 'bold',
    fontSize: 18,
    marginBottom: '5%',
    marginLeft:210,
    right:10
  },
  btnTextnew:{
    color:"#fff",
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: '5%'
    
  },
  homebutton:{
    flexDirection: "row",
    marginTop:10,
    justifyContent: "center",
    alignItems: "center"
  },
  newbutton:{
    flexDirection: "row",
    // marginTop:10,
    // justifyContent: "center",
    // alignItems: "center"
    marginTop:40,
    justifyContent: "center",
    alignItems: "center"
  },
  newbutton2:{
    flexDirection: "row",
   // marginTop:40
   paddingTop:10,
   justifyContent: "center",
    alignItems: "center"
  },
  newbutton3:{
    flexDirection: "row",
   // marginTop:40
   paddingTop:20,
   justifyContent: "center",
    alignItems: "center"
  },
  location: {
    fontSize: 18,
    color: '#101010',
    justifyContent: "flex-start",
    marginLeft:5,
  //  padding: 5,
  //  fontWeight: '700'
  
  },
  icon:{
    // marginLeft:40,
    color:"white",
    left: 45,
    justifyContent: "center",
    alignItems: "center",
    bottom: 5
  }
}); 
//  console.log(">>Data" +JSON.stringify(data));
  //   return(<FlatList
  //     data={data}
  //     keyExtractor={item => item.id}
  //     renderItem = {({ item }) => <View>
  //       <Text>hello world</Text>
  //       </View>
  //   }
      
  //   /> )