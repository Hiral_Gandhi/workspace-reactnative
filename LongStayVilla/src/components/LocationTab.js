import React ,{ useEffect, useState }from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator,TextInput, Image, Button,TouchableOpacity, ImageBackground, Dimensions, ScrollView} from 'react-native';
var screenHeight = Dimensions.get("screen").height;
import {fetchData} from '../service/api'
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconEntypo from 'react-native-vector-icons/Entypo';


const data = [
  { id: '1', image: require('../assets/Image4.jpg')},
  { id: '2', image: require('../assets/Image5.jpg')},
  { id: '3', image: require('../assets/Image6.jpg') }
];
const LocationTab = ({navigation}) => {
  //   return (
  //     <View style={styles.container}>
  //     {/* <Text style={styles.text}>Basic FlatList Example</Text> */}
  //     <FlatList
  //       data={data}
  //       keyExtractor={item => item.id}
  //       renderItem={({ item }) => (
  //         <View style={styles.listItem}>
  //           <Image style={styles.listItem} source={item.image}/>
              
  //         </View>
  //       )}
  //     />
  //   </View>
  // );
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetchData().then((result)=>{
        console.log("result",result)
        setData(result)
        setLoading(false)
    })
  },[])
   
  return (
    <View style={styles.container}>
      <View style={{ flexDirection: "row", backgroundColor: "white", width: "100%", justifyContent: "space-around" }}>
          {/* <View>
          <IconEntypo name="chevron-left" size={27} style={{ color: "#ff6969", marginTop: "27%" }} />
          </View> */}
          <View style={styles.SectionStyle}>
          <Image source={require('../assets/SearchLogo.png')} style={styles.ImageStyle} />
          <TextInput
          style={styles.inputText}
          placeholder="Search"
          placeholderTextColor="#696969"
          />
          </View>
          <View>
          <IconAntDesign name="filter" size={25} style={{ marginTop: "25%", marginLeft: "5%", color: "grey" }} />
          </View>
          </View>


      {isLoading ? <ActivityIndicator/> : (
        <FlatList style={styles.listItem}
        data={data}
        keyExtractor={item => item.id}
        renderItem={({ item }) => (

            <View >
            {/* {console.log('item',item.image)} */}
                    <Image style={styles.listItem} source={{uri:item.image}}/>
                    {/* <Text style={styles.text}>{item.title}</Text>
                    <Text style={styles.price}>{item.price}</Text>
                    <View style={styles.homebutton}>
                    <Text style={styles.location}>{item.Location}</Text>
                    <TouchableOpacity style={styles.moreBtn} onPress={() => alert("pressed!")}>
                        <Text style={styles.btnText}>MORE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bookvillaBtn} onPress={() => navigation.navigate('NewBooking')}>
                        <Text style={styles.btnText}>Book Villa</Text>
                    </TouchableOpacity>
                    </View>   */}
                  </View>
                )}
              /> 
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    height:screenHeight/3.7,
    padding: 5,
    marginBottom:10,
   // alignItems: 'center',
  //  borderColor:"black",
  //  borderWidth:1,
    backgroundColor: '#fff',
    width: '100%'
  
  },
 
  listItem: {
  //  marginTop: 10,
    flex: 1,
    height:screenHeight/3.7,
    padding: 5,
    marginBottom:10,
   // alignItems: 'center',
  //  borderColor:"black",
  //  borderWidth:1,
    backgroundColor: '#fff',
    width: '100%'
  },
  SectionStyle: {
    flexDirection: 'row',
    // borderWidth: 1,
    borderColor: '#A9A9A9',
    height: 40,
    borderRadius: 20,
    margin: 5,
    alignItems: 'center',
   // justifyContent:"center",
    width: "85%",
    backgroundColor: "#F2F2F2",
     marginLeft: "5%"
    },
    ImageStyle: {
      padding: 10,
      margin: 10,
      height: 20,
      width: 20,
      resizeMode: 'stretch',
      alignItems: 'center'
      },
      inputText: {
        // height: 50,
        color: "#454545",
        fontSize: 18,
        }
  
}); 
export default LocationTab;