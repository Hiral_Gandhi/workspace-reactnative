import React ,{ useEffect, useState }from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator, Image, Button,TouchableOpacity, ImageBackground, Dimensions, ScrollView} from 'react-native';
import {fetchData} from '../service/api'
//import NewBooking from './src/components/NewBooking';
import Iconloc from 'react-native-vector-icons/Entypo';
var screenHeight = Dimensions.get("screen").height;
//import  { useState } from 'react';
// const dataimage=()=>{
// fetch('http://training.pyther.com/long-stay-villa/', {
//   method: 'GET ',
//   headers: {
//     Accept: 'application/json',
//     'Content-Type': 'application/json'
//   },
//   body: JSON.stringify({
//   //  firstParam: 'yourValue',
//   //  secondParam: 'yourOtherValue'
//   })
// });
// }

// const data = [
//   { id: '1', image: require('../assets/Image1.jpg'),
//   title: 'Villa Jane will astonish every guest with its breathtaking views',price:'$2000' },
//   { id: '2', image: require('../assets/Image2.jpg'),
//   title:'An ultra-exclusive mountain home Ranch-inspired luxury living',price:'$3500' },
//   { id: '3', image: require('../assets/Image3.jpg'),title: 'Villa Jane will astonish every guest with its breathtaking views',price:'$2000' }
// ];




// const HomeTab = ({navigation}) => {
//   // const [images, setImages] =  useState(null);

//   // var fetchData = async () => {
//   //   const apiURL = "http://localhost:3000/api/long-stay";
//   //   const response = await fetch(apiURL, {
//   //     method: "GET",
//   //     headers: {
//   //     Accept: "application/json",
//   //     "Content-Type": "application/json",
//   //     }
//   //   });
//   // //  console.log(response);
//   //   setImages(response.data);
//   // };

//   //   fetchData();
//     return (
//       <View style={styles.container}>
//       {/* <Text style={styles.text}>Basic FlatList Example</Text> */}
//       <FlatList
//         data={data}
//         keyExtractor={item => item.id}
//         renderItem={({ item }) => (
          
          // <View style={styles.listItem}>
          //   <Image style={styles.listItem} source={item.image}/>
          //   <Text style={styles.text}>{item.title}</Text>
          //   <Text style={styles.price}>{item.price}</Text>
          //   <View style={styles.homebutton}>
          //   <TouchableOpacity style={styles.moreBtn} onPress={() => alert("pressed!")}>
          //       <Text style={styles.btnText}>MORE</Text>
          //   </TouchableOpacity>
          //   <TouchableOpacity style={styles.bookvillaBtn} onPress={() => navigation.navigate('NewBooking')}>
          //       <Text style={styles.btnText}>Book Villa</Text>
          //   </TouchableOpacity>
          //   </View>  
          // </View>
//         )}
//       />
//     </View>
//   );
// };
export default HomeTab = ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetchData().then((result)=>{
        console.log("result",result)
        setData(result)
        setLoading(false)
    })
  },[])
   

  return (
    <View style={styles.listItem}>
      {isLoading ? <ActivityIndicator/> : (
        <FlatList style={styles.listItem}
        data={data}
        keyExtractor={item => item.id}
        renderItem={({ item }) => (

            <View >
            {/* {console.log('item',item.image)} */}
                    <Image style={styles.listItem} source={{uri:item.image}}/>
                    <Text style={styles.text}>{item.title}</Text>
                    <Text style={styles.price}>{item.price}</Text>
                    <View style={styles.homebutton}>
                    <Iconloc name={'location-pin'} size={25} />
                    <Text style={styles.location}>{item.Location}</Text>
                    <TouchableOpacity style={styles.moreBtn} onPress={() => { navigation.navigate('Location')}}>
                        <Text style={styles.btnText}>MORE</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bookvillaBtn} onPress={() =>{ navigation.navigate('NewBooking',{id:item.id})}}>
                        <Text style={styles.btnText}>Book Villa</Text>
                    </TouchableOpacity>
                    </View>  
                  </View>
                )}
              /> 
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
   // alignItems: 'center'
  },
  text: {
    fontSize: 18,
    color: '#101010',
    justifyContent: "flex-start",
    padding: 5,
  //  fontWeight: '700'
  
  },
  price: {
    fontSize: 18,
    color: '#B2002D',
    justifyContent: "flex-start",
    marginLeft:5,
  //  padding: 5,
  //  fontWeight: '700'
  
  },
  listItem: {
  //  marginTop: 10,
    flex: 1,
    height:screenHeight/4.2,
    padding: 5,
    
   // alignItems: 'center',
  //  borderColor:"black",
  //  borderWidth:1,
   // backgroundColor: '#fff',
    width: '100%'
  },
  listItemText: {
    fontSize: 18
  },
  bookvillaBtn: {
   // flexDirection: "row",
   marginLeft:10,
    width: "31%",
    borderRadius: 25,
    height: 27,
    alignItems: "center",
    justifyContent: "center",
 //   marginTop: 5,
    backgroundColor: "#B2002D",
    marginBottom:10,
  },
  moreBtn: {
  //  flexDirection: "row",
    marginLeft:90,
    width: "20%",
    borderRadius: 25,
    height: 27,
    alignItems: "center",
    justifyContent: "center",
 //   marginTop: 15,
    backgroundColor: "#B2002D",
  //    marginBottom:10,
  },
  btnText:{
    color:"#fff",
    fontWeight: 'bold',
    fontSize: 15,
    
  },
  homebutton:{
    flexDirection: "row",
    marginTop:10
  },
  location: {
    fontSize: 18,
    color: '#101010',
    justifyContent: "flex-start",
  //  marginLeft:5,
  //  padding: 5,
  //  fontWeight: '700'
  
  },
}); 
//export default HomeTab;

// ,{id:item.id}