import { StyleSheet, Text, View, Image, Button,TouchableOpacity, TextInput, ScrollView} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React, { useState } from 'react';

function Login({ navigation }) {
  const [email, setEmail] = useState('');
     const [password, setPassword] = useState('');
    return (
  <View style={styles.container}>
  <ScrollView>
    <Text style={styles.black}>Login Account</Text>
    <Text style={styles.textEmail}>Email</Text>
    
      <View style={styles.inputView}>
          <TextInput
              style={styles.TextInput}
              onChangeText={(email) => setEmail(email)}
          />
      </View>
      <Text style={styles.textPwd}>password</Text>
      <View style={styles.pwdView}>
          <TextInput
              style={styles.TextInput}
              secureTextEntry={true}
              onChangeText={(password) => setPassword(password)}
          />
      </View>
      <View>
      <TouchableOpacity style={styles.loginBtn} onPress={() => navigation.navigate('BookVilla')}>
        <Text style={styles.btnText}>LOGIN</Text>
      </TouchableOpacity>
      </View>
      <TouchableOpacity style={styles.forgotbutton}  >
        <Text style={styles.btnfrgt}>FORGOT PASSWORD</Text>
      </TouchableOpacity>
      </ScrollView>
  </View>
    );
  }

  const styles = StyleSheet.create({
    container: {
      
         flex:1,
         backgroundColor: "white",
       },

    black: {
        marginTop: 100,
        marginLeft:30,
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
      },
    textEmail:{
      marginTop: 50,
      marginLeft:30,
      color: 'black',
      fontSize: 15,
    
    },

    textPwd:{
      marginTop: 20,
      marginLeft:30,
      color: 'black',
      fontSize: 15,
      
    },
    inputView: {
      marginTop: 10,
      marginLeft:30,
      backgroundColor: "white",
      borderRadius: 30,
      borderColor: "gray",
      borderWidth:1,
      width: "85%",
      height: 50,
    //  marginBottom: 20,
     // alignItems: "center",
    },
    
    pwdView: {
      marginTop: 10,
      marginLeft:30,
      backgroundColor: "white",
      borderRadius: 30,
      borderColor: "gray",
      borderWidth:1,
      width: "85%",
      height: 50,
      marginBottom: 20,
    //  alignItems: "center",
    },

    TextInput: {
      height: 50,
     // flex: 1,
      padding: 10,
      marginLeft: 20,
    },

    loginBtn: {
      marginLeft:30,
      width: "85%",
      borderRadius: 25,
      height: 50,
      alignItems: "center",
      justifyContent: "center",
      marginTop: 20,
      backgroundColor: "#B2002D",
    },

    btnText:{
      color:"#fff",
      fontWeight: 'bold',
    },

    forgotbutton: {
      marginTop: 25,
    //  right:20,
      height: 30,
      marginBottom: 30,
      alignItems: "center",
      justifyContent: "center",
    },

    btnfrgt:{
      color:"#B2002D",
      fontWeight: 'bold',
      
    },
  });





  export default Login;