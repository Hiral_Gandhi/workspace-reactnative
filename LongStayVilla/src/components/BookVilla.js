import React from 'react';
 import { StyleSheet, Text, View, Image, Button,TouchableOpacity} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
 import HomeTab from './HomeTab';
 import LocationTab from './LocationTab';
 import BookingsTab from './BookingsTab';
 import ProfileTab from './ProfileTab';
 import MoreTab from './MoreTab';
 import Login from './Login';
 import { createAppContainer } from 'react-navigation';
 import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
//  import homeIcon from 'react-native-vector-icons/AntDesign';
 import Icon from 'react-native-vector-icons/Feather'; 
 import SimpleIcon from 'react-native-vector-icons/SimpleLineIcons';
 import Ionicons from 'react-native-vector-icons/Ionicons';

 

//const Stack = createStackNavigator();
 const AppNavigator = createBottomTabNavigator(
//   {
//   Home: {
//     screen: HomeTab,
//     navigationOptions:{  
//       tabBarLabel:'Home',
//       tabBarOptions: { 
//         activeTintColor: '#B2002D',
//       },  
//       tabBarIcon: ({ tintColor }) => (  
//         <Image       
//         source={require('../components/HomeIcon.png')}
//         style={[styles.icon, { tintColor: tintColor }]}
//       />
//       ),  
//   }     
//   },
//   Location: {
//     screen: LocationTab,
//     navigationOptions:{  
//       tabBarLabel:'Location', 
//       tabBarOptions: { 
//         activeTintColor: '#B2002D',
//       },  
//       tabBarIcon: ({ tintColor }) => (  
//         <Image
//         source={require('../components/LocationIcon.png')}
//         style={[styles.icon, { tintColor: tintColor }]}
//       />
//       ),  
//   }     
//   },
//   Bookings: {
//     screen: BookingsTab,
//     navigationOptions:{  
//       tabBarLabel:'Bookings', 
//       tabBarOptions: { 
//         activeTintColor: '#B2002D',
//       },  
//       tabBarIcon: ({ tintColor }) => (  
//         <Image
//         source={require('../components/BookingsIcon.png')}
//         style={[styles.icon, { tintColor: tintColor }]}
//       />
//       ),  
//   }     
//   },
//   Profile: {
//     screen: ProfileTab,
//     navigationOptions:{  
//       tabBarLabel:'Profile',  
//       tabBarOptions: { 
//         activeTintColor: '#B2002D',
//       }, 
//       tabBarIcon: ({ tintColor }) => (  
//         <Image
//         source={require('../components/ProfileIcon.png')}
//         style={[styles.icon, { tintColor: tintColor }]}
//       />
//       ),  
//   }     
//   },
//   More: {
//     screen: MoreTab,
//     navigationOptions:{  
//       tabBarLabel:'More',  
//       tabBarOptions: { 
//         activeTintColor: '#B2002D',
//       }, 
//       tabBarIcon: ({ tintColor }) => (  
//         <Image
//         source={require('../components/MoreIcon.png')}
//         style={[styles.icon, { tintColor: tintColor }]}
//       />
//       ),  
//   }     
//   }
// }, {
//   initialRouteName: "Home"
// }
);
const AppContainer = createAppContainer(AppNavigator);
const BookVilla = ({navigation }) => {
     return (
      // <NavigationContainer>
      <AppNavigator.Navigator
      
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === 'Home') {
            return <Icon name="home" size={size} color={color} />;
          } 
          else if (route.name === 'Location') {
            return <Icon name="search" size={size} color={color} />;
          }
          else if (route.name === 'Bookings') {
            return <Icon name="heart" size={size} color={color} />;
          }
          else if (route.name === 'Profile') {
            return <SimpleIcon name="user" size={size} color={color} />;
          }
          else if (route.name === 'More') {
            return <Icon name="list" size={size} color={color} />;
          }

          // You can return any component that you like here!
          // return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: '#B2002D',
   //    inactiveTintColor: 'gray',
      }}
    >
        <AppNavigator.Screen name="Home" component={HomeTab} />
        <AppNavigator.Screen name="Location" component={LocationTab} />
        <AppNavigator.Screen name="Bookings" component={BookingsTab} />
        <AppNavigator.Screen name="Profile" component={ProfileTab} />
        <AppNavigator.Screen name="More" component={MoreTab} />
      </AppNavigator.Navigator>
    //  <AppContainer/>
   // </NavigationContainer>      
     );
}


// const AppNav = createStackNavigator({
//   Login: {screen: Login},
//  Article: {screen: Article},
 // },
  // {
  //     // Specifing Initial Screen
  //     initalRoute: 'Home'
  // }
//);

//const App = createAppContainer(AppNav);


const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24,
  }
});


 export default BookVilla;