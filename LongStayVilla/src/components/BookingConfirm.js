import React from 'react';
import { StyleSheet, Text, View, Image, Button,TouchableOpacity, ImageBackground, Dimensions, ScrollView} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './Login';
import Icon from 'react-native-vector-icons/AntDesign';
var screenHeight = Dimensions.get("screen").height;
var screenWidth = Dimensions.get("screen").width;


 const BookingConfirm = ({ navigation }) => {
    return (
      <View >
        <ScrollView style={height="90%", backgroundColor="white"}>
        <View >
        <View style={styles.container}>
        <Icon name={"checkcircleo"} style={styles.icon} size={100} />
        </View>
        <View style={styles.container}>
           <Text style={styles.bigfont}>Booking Confirm</Text>
           </View>
           <View style={styles.container}>
           <Text style={styles.text}>We will call you for more information in 24 Hours.</Text>
           <Text style={styles.text}>Thanks for Booking !!</Text>
           <View style={{marginTop:30}}>
           <TouchableOpacity style={styles.moreBtn} onPress={() => navigation.navigate('Home')}>
                    <View style={styles.homebutton}>
                        <Text style={styles.btnText}>View More</Text>
                        
                        <Icon name={'rightcircle'} style={styles.iconview} size={33} />
                        </View>   
                    </TouchableOpacity>
                    </View>
           </View>
           </View>
           </ScrollView>
           </View>
    );
};
const styles = StyleSheet.create({
    container: {
        //   marginTop: 30,
         //  backgroundColor: "white",
        // flex:1,
         alignItems: "center",
         justifyContent: "center",
         paddingLeft:50,
         paddingRight:50
         },
    bigfont:{
        fontSize:40,
        marginTop:40,
       // marginLeft:55,
        color:"#696969"
    },
    text:{
        fontSize:20,
        color:"#696969",
        marginTop:20, 
        textAlign:"center",
       // marginLeft:55,
      
    },
    icon:{
        alignItems: "center",
         justifyContent: "center",
        //  paddingLeft:150,
        //  paddingRight:150,
          paddingTop:120,
        color:"#B2002D"
    },
    moreBtn: {
      //  flexDirection: "row",
     //   marginLeft:110,
        width: 180,
        borderRadius: 30,
        height: 55,
        alignItems: "center",
        justifyContent: "center",
     //   marginTop: 15,
        backgroundColor: "#B2002D",
     //  marginBottom:10,
      },
      btnText:{
        color:"#fff",
        fontWeight: 'bold',
        fontSize: 16,
        marginBottom: '5%'
        
      },
      homebutton:{
        flexDirection: "row",
        marginTop:10,
        justifyContent: "center",
        alignItems: "center"
      },
    iconview:{
           // marginLeft:20,
            color:"white",
            marginBottom:"5%",
            left: 20,
            justifyContent: "center",
            alignItems: "center",
          
       //     paddingBottom:60
            
          }
});
export default BookingConfirm;