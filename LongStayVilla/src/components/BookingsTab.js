import React ,{ useEffect, useState }from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator, Image, Button,TouchableOpacity, ImageBackground, Dimensions, ScrollView} from 'react-native';
//import {fetchDetails} from '../service/apidetails';
//import NewBooking from './src/components/NewBooking';
//import {fetchData} from '../service/api';
var screenHeight = Dimensions.get("screen").height;
var screenWidth = Dimensions.get("screen").width;
import Iconloc from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/AntDesign';

const fetchData = () => {
  // const id  = route.params;
 // console.log("fetchDetails "+id);
//   const id = props.match.params.id;
   const apiURL = "https://nodeapi.pyther.com/api/long-stay/7";
 // console.log('hi',apiURL);
   return fetch(apiURL, {
     method: "GET",
     headers: {
     Accept: "application/json",
     "Content-Type": "application/json",
     }
   }).then((res)=> res.json())
   .then((result)=>{
       return result
       
   },(error)=>{
       console.log(">>>err",error)
       return error;
   })
 };
export default BookingsTab = ({navigation }) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
   
    fetchData().then((result)=>{
        console.log("result",result)
        setData(result)
        setLoading(false)
    })
    
    
  },[])
  
  return (
    <ScrollView style={{backgroundColor:"white",flex:1}}>
        {/* {console.log('item'+isLoading)}    */}
      {isLoading ? <ActivityIndicator/> : (
          <View>
            {/* {console.log('item',item)} */}
            
            <Image style={styles.listItem} source={{uri:data.image}}/>
            <View style={styles.homebutton}>
                  <Text style={styles.textnew}>{data.title}</Text>
            </View>
            
            <View style={styles.container2}>
            <View style={styles.homebutton}>
                  <Text style={styles.btnText7}>Price {data.price} / Per Day</Text>
                  <Text style={styles.btnText2}>Total : $24000</Text>
            </View>
                  <View style={styles.homebutton}>
                  <Text style={styles.btnText1}>Check In Date : 12th April 2021</Text>
                  </View>
                  <View style={styles.homebutton}>
                  <Text style={styles.btnText8}>Check Out Date : 18th April 2021</Text>
                  <Iconloc name={'location-pin'} style={styles.iconloc} size={25} />
                  <Text style={styles.btnText2}>{data.Location}</Text>
                  </View>
            </View>
                    <View style={styles.homebutton}>
                    <TouchableOpacity style={styles.moreBtn} onPress={() => navigation.navigate('Home')}>
                    <View style={styles.homebutton}>
                        <Text style={styles.btnTextnew}>Book More</Text>
                        <Icon name={'rightcircle'} style={styles.icon} size={33} />
                    </View>   
                    </TouchableOpacity> 
                    </View>                  
          </View>   
      )} 
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f8f8f8',
   // alignItems: 'center'
  },
  container2: {
    marginTop:20,
    left:5
  },
  containernew: {
    //   marginTop: 30,
     //  backgroundColor: "white",
    // flex:1,
    // alignItems: "center",
   //  justifyContent: "center",
    marginLeft:10,
   //  paddingRight:10
     },
  text: {
    fontSize: 18,
    color: '#101010',
    justifyContent: "flex-start",
    padding: 5,
  //  fontWeight: '700'
  
  },
  homebutton:{
    flexDirection: "row"
    },
  textnew:{
    fontSize:19,
 //   color:"#696969",
    marginTop:30, 
   // textAlign:"center",
   // marginLeft:55,
  //right:10
},
  price: {
    fontSize: 18,
    color: '#B2002D',
    justifyContent: "flex-start",
    marginLeft:5,
  //  padding: 5,
  //  fontWeight: '700'
  
  },
  listItem: {
    marginTop: 20,
    //marginLeft:80,
   // flex: 1,
     height:screenHeight/3.5,
    left: 5,
   // right:10,
   justifyContent: "center",
    alignItems: 'center',
  //  borderColor:"black",
  //  borderWidth:1,
   // backgroundColor: '#fff',
    width: '98%'
  },
  listItemText: {
    fontSize: 18
  },
  bookvillaBtn: {
   // flexDirection: "row",
   marginLeft:10,
    width: "31%",
    borderRadius: 25,
    height: 27,
    alignItems: "center",
    justifyContent: "center",
 //   marginTop: 5,
    backgroundColor: "#B2002D",
    marginBottom:10,
  },
  moreBtn: {
  //  flexDirection: "row",
  //  marginLeft:40,
   //left:95,
    marginTop:40,
    width: screenWidth/2,
    borderRadius: 30,
    height: 53,
    alignItems: "center",
    justifyContent: "center",
 //   marginTop: 15,
    backgroundColor: "#B2002D",
 //  marginBottom:10,
  },
  btnText:{
  //  color:"grey",
   // fontWeight: 'bold',
    fontSize: 18,
    marginBottom: '5%',
    marginLeft:10
    
  },
  btnText1:{
    //  color:"grey",
     // fontWeight: 'bold',
      fontSize: 18,
      marginBottom: '5%',
   //   marginLeft:5,
      marginRight:135,
    },
  btnText2:{
  //  color:"grey",
   // fontWeight: 'bold',
    fontSize: 18,
    bottom:10
    //top:10,
   
  },
  btnText3:{
  //  color:"grey",
   // fontWeight: 'bold',
    fontSize: 18,
    marginBottom: '5%',
    marginLeft:85
  },
  btnText4:{
  //  color:"grey",
   // fontWeight: 'bold',
    fontSize: 18,
    marginBottom: '5%',
    marginLeft:260,
    left:10
  },
  btnText5:{
   // color:"grey",
   // fontWeight: 'bold',
    fontSize: 14,
    marginBottom: '5%',
    marginLeft:10
    
  },
  btnText6:{
  //  color:"grey",
   // fontWeight: 'bold',
    fontSize: 18,
    marginBottom: '5%',
    marginLeft:210,
    right:10
  },
  btnText7:{
  right:40,
  fontSize: 18,
  bottom:10
  },
  btnText8:{
    right:20,
    fontSize: 18,
    bottom:10
    },
  btnTextnew:{
    color:"#fff",
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: '5%'
    
  },
  homebutton:{
    flexDirection: "row",
    marginTop:10,
    justifyContent: "center",
    alignItems: "center"
  },
  newbutton:{
    flexDirection: "row",
    // marginTop:10,
    // justifyContent: "center",
    // alignItems: "center"
    marginTop:40
  },
  newbutton2:{
    flexDirection: "row",
   // marginTop:40
   paddingTop:10
  },
  newbutton3:{
    flexDirection: "row",
   // marginTop:40
   paddingTop:20
  },
  location: {
    fontSize: 18,
    color: '#101010',
    justifyContent: "flex-start",
    marginLeft:5,
  //  padding: 5,
  //  fontWeight: '700'
  
  },
  icon:{
    // marginLeft:40,
    color:"white",
    left: 25,
    justifyContent: "center",
    alignItems: "center",
    bottom:5
  },
  iconloc:{
    // marginLeft:40,
  //  color:"white",
    left: 5,
    justifyContent: "center",
    alignItems: "center",
    bottom:10
  }
}); 