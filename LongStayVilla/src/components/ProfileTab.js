import React from 'react';
import { StyleSheet, Text, View,FlatList, Image, Button,TouchableOpacity, ImageBackground, Dimensions, ScrollView} from 'react-native';
import { Item } from 'react-native-paper/lib/typescript/components/List/List';
import { color } from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/AntDesign';
//import Icon from 'react-native-vector-icons/Feather';
var screenHeight = Dimensions.get("screen").height;
import Login from './Login';
const data = [
  { id: '1',image: require('../assets/MoreIcon.png'),title: 'All My Booking',text:'Home',name:'rightcircle'},
  { id: '2',image: require('../assets/Pending-visits.png'),title: 'Pending Visits',text:'Home',name:'rightcircle'},
  { id: '3',image: require('../assets/Pending-Payments-logo.png'),title: 'Pending Payments',text:'Home',name:'rightcircle'},
  { id: '4',image: require('../assets/Feedback.png'),title: 'Feedback',text:'Home',name:'rightcircle'}
];

const ProfileTab = ({navigation}) => {
    return (
      <ScrollView style={height="90%", backgroundColor="white"}>
      <View > 
       
          <View style={styles.main}>
         <View style={styles.containernew}>
         <Image
            source={require('../assets/Jane-Doe.jpg')}
            style={[styles.icon]}
            />
             </View>
          <View style={styles.containernew}>
         <Text style={styles.nametext}>Jane Doe</Text>
         <Text style={styles.emailtext}>janedoe123@email.com</Text>
            <TouchableOpacity style={styles.logoutBtn} onPress={() => navigation.navigate('Home')}>
            <Text style={styles.btnText}>EDIT PROFILE</Text>
            </TouchableOpacity> 
            </View>
           
      </View> 
      <View style={styles.container}>
     
            <View style={styles.homebutton}>
            <TouchableOpacity style={styles.addBtn} onPress= {() => navigation.navigate('Bookings')}>
            <View style={styles.homebutton}>
            {/* <Icon style={styles.btn} name={item.iconName}  /> */}
            <Image style={styles.btn} source={require('../assets/MoreIcon.png')}/>
            <Text style={{width:240,fontSize:18,color:"gray",marginLeft:20}}>{'All My Booking'}</Text>
                <View style={{alignItems:'flex-end'}}>
                <Icon name={'rightcircle'} style={{color:"#cccccc"}} size={20} />
                </View>

            </View>
            </TouchableOpacity>
            </View>  
         
            <View style={styles.homebutton}>
            <TouchableOpacity style={styles.addBtn} onPress= {() => navigation.navigate('Location')}>
            <View style={styles.homebutton}>
            {/* <Icon style={styles.btn} name={item.iconName}  /> */}
            <Image style={styles.btn} source={require('../assets/Pending-visits.png')}/>
            <Text style={{width:240,fontSize:18,color:"gray",marginLeft:20}}>{'Pending Visits'}</Text>
            {/* <View> */}
                <View style={{alignItems:'flex-end'}}>
                <Icon name={'rightcircle'} style={{color:"#cccccc"}} size={20} />
                </View>
            {/* </View> */}
            </View>
            </TouchableOpacity>
            </View>  
         
            <View style={styles.homebutton}>
            <TouchableOpacity style={styles.addBtn} onPress= {() => navigation.navigate('Bookings')}>
            <View style={styles.homebutton}>
            {/* <Icon style={styles.btn} name={item.iconName}  /> */}
            <Image style={styles.btn} source={require('../assets/Pending-Payments-logo.png')}/>
            <Text style={{width:240,fontSize:18,color:"gray",marginLeft:20}}>{'Pending Payments'}</Text>
            {/* <View> */}
                <View style={{alignItems:'flex-end'}}>
                <Icon name={'rightcircle'} style={{color:"#cccccc"}} size={20} />
                </View>
            {/* </View> */}
            </View>
            </TouchableOpacity>
            </View>  

            <View style={styles.homebutton}>
            <TouchableOpacity style={styles.addBtn} onPress= {() => navigation.navigate('Home')}>
            <View style={styles.homebutton}>
            {/* <Icon style={styles.btn} name={item.iconName}  /> */}
            <Image style={styles.btn} source={require('../assets/Feedback.png')}/>
            <Text style={{width:240,fontSize:18,color:"gray",marginLeft:20}}>{'Feedback'}</Text>
            {/* <View> */}
                <View style={{alignItems:'flex-end'}}>
                <Icon name={'rightcircle'} style={{color:"#cccccc"}} size={20} />
                </View>
            {/* </View> */}
            </View>
            </TouchableOpacity>
            </View>  
      </View>
      
    </View> 
    </ScrollView>
    );
};
const styles = StyleSheet.create({
  container: {  
    height: 200,
    width: "90%",
    borderRadius: 13,
    backgroundColor: "white",
    justifyContent: "space-around",
    marginTop: "5%",
    marginLeft:20
     },
  containernew:{
    marginTop:30,
   // flex:2,
  },
  addBtn: {    
      flex:1,
      backgroundColor: "white",
      },
  // paymentBtn: {    
  //     flex:1,
  //     backgroundColor: "white",
  //     },
  // currencyBtn: {    
  //     flex:1,
  //     backgroundColor: "white",
  //     },
  // languageBtn: {    
  //     flex:1,
  //     backgroundColor: "white",
  //     },
  listItem: {
          marginTop: 10,
        //  flex: 1,
          height:screenHeight/19,
          padding: 6,
          borderBottomColor:"#d3d3d3",
          borderBottomWidth:1,
          marginLeft:15,
          right:15,
         // alignItems: 'center',
        //  borderColor:"black",
        //  borderWidth:1,
         // fontSize:100,
          backgroundColor: '#fff',
          width: '100%'
        },
    homebutton:{
          flexDirection: "row",
          
        },
    text: {
          fontSize: 18,
          color: 'grey',
          justifyContent: "flex-start",
          paddingBottom: 10,
          marginLeft:30
          
        //  fontWeight: '700'
        
        },
    logoutBtn: {
      marginLeft:40,
      width: "60%",
      borderRadius: 25,
      height: 32,
      alignItems: "center",
      justifyContent: "center",
      borderWidth:1,
      borderColor:'#d3d3d3',
      marginTop: 15,
   //   backgroundColor: "#f0f8ff",
      marginBottom:10,
        },
    
        btnText:{
         // color:"#B2002D",
         // fontWeight: 'bold',
         color:"#696969",
        fontWeight: 'bold',
        fontSize: 14,
        },

        btn:{
          width: 24,
          height: 24,
         // color: '#d3d3d3',
           tintColor:'grey',
          // size:24
          marginLeft:10
         
        },
        btn2:{
          width: 10,
          height: 10,
          //color: '#d3d3d3',
         // tintColor:'grey',
          
          marginTop:5,
          marginLeft:5
        },
        homebutton:{
          flexDirection: "row",
        },
        arrowbutton:{
         // flexDirection:"column",
       //  marginLeft:120
       width:20,
       height:20,
       
      // paddingLeft:10,
       
        // padding:10,
          borderRadius:360,
         // borderWidth:1
        },
        nametext:{
          color:"#696969",
          fontWeight: 'bold',
          fontSize:38,
          marginLeft:40,
          marginBottom:10
        },
        emailtext:{
          color:"#696969",
          //fontWeight: 'bold',
          fontSize:18,
          marginLeft:40,
          marginTop:10
        },
        icon:{
          borderRadius:360,
          marginLeft:20,
          width:120,
          height:120
        },
        main:{
          flexDirection: "row",
        },
    });
export default ProfileTab;