import React from 'react';
import { StyleSheet, Text, View, Image, Button,TouchableOpacity, ImageBackground, Dimensions, ScrollView} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './Login';
var screenHeight = Dimensions.get("screen").height;
var screenWidth = Dimensions.get("screen").width;


const Homepage = ({ navigation }) => {
    return (
      <View style={styles.container}>
        <ScrollView style={height="90%", backgroundColor="white"}>
        <View style={styles.containernew}>
          
           <Text style={[styles.bigBlue, styles.red]}>LongStay  Villa</Text>
           <ImageBackground
            source={require('../assets/004_villa.jpg')}
            style={styles.backgroundImage}

            />         
         </View>
         <View style={styles.containernew}>
        <TouchableOpacity style={styles.startBtn} 
          onPress={() => navigation.navigate('Login')}>
            <Text style={styles.btnText}>GET STARTED
            <Image
            source={require('../assets/ArrowIcon.png')}
            style={[styles.icon, { tintColor: 'white' }]}
            /></Text>
            
        </TouchableOpacity>
        </View>
        </ScrollView>
      </View>
    );
};
  
  const styles = StyleSheet.create({
    container: {
   //   marginTop: 30,
      backgroundColor: "white",
      
    },
    containernew:{
      alignItems: "center",
      justifyContent: "center",
    },
    bigBlue: {
      color: 'blue',
      fontWeight: 'bold',
      fontSize: 55,
   //   textAlign:'center',
      marginLeft: 5,
    },
    red: {
      color: '#B2002D',
      
    //  fontFamily:"Montserrat-Regular"
    },
    backgroundImage:{

      flex:1,
      resizeMode:"contain",
      height:screenHeight/1.5,
      width: '100%',
      justifyContent: "center",
    },
    startBtn: {
   marginTop:20,
   marginLeft:230,
    paddingHorizontal: 30,
    paddingVertical: 20,
    borderRadius: 30,
    backgroundColor: "#B2002D",
    alignSelf: "flex-start",
    marginHorizontal: "60%",
    marginBottom: 4,
   // minWidth: "50%",
   width:screenWidth/2,
   // textAlign: "left",
   
    
    },
    alternativeLayoutButtonContainer: {
      margin: 20,
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
    btnText:{
      color:"#fff",
      fontSize:17,
    },
    icon:{
      width: 20,
    height: 20,
    
    },
  });
  
export default Homepage;