export const fetchData = () => {
    const apiURL = "https://nodeapi.pyther.com/api/book";
    return fetch(apiURL, {
    method: "GET",
    headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
    }
    }).then((res)=> res.json())
    .then((result)=>{
    return result
    },(error)=>{
    console.log(">>>err",error)
    return error;
    })
    };
