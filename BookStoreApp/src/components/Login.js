import { StyleSheet, Text, View, Image, Button,TouchableHighlight,  TouchableOpacity,Dimensions, TextInput, ScrollView} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React, { useState } from 'react';
var screenHeight = Dimensions.get("screen").height;
var screenWidth = Dimensions.get("screen").width;

const Login=({ navigation })=> {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [selected, setselected] = useState(null);
  const [selectedButton, setselectedButton] = useState('');

  const _handleClick = (flag, button)=> {
    if (flag == 1) {
      setselected(true);
    }
    setselectedButton(button);
  }

return (
  <View style={styles.container}>
  <ScrollView>
  <View style={styles.containersignin}>
      <TouchableHighlight  style={styles.signinBtn} >
     
        <Text style={styles.textsignin}>SIGN IN</Text>
       
      </TouchableHighlight>
      <TouchableHighlight  onPress={() => _handleClick('any flag', '1')} underlayColor="#6200EE">
      <View style={{backgroundColor: (selectedButton === '1' ? 'style={styles.signinBtn}' : '')}}>
        <Text style={styles.textsignup}>SIGN UP</Text>
        </View>
      </TouchableHighlight>
      </View>
      <View style={styles.inputView}>
          <TextInput
           placeholder = "Email" 
              style={styles.TextInput}
              onChangeText={(email) => setEmail(email)}
          />
      </View>
      
      <View style={styles.pwdView}>
          <TextInput
              style={styles.TextInput}
              placeholder = "Password" 
              secureTextEntry={true}
              onChangeText={(password) => setPassword(password)}
          />
      </View>
      <View style={styles.containercontinue}>
      <TouchableOpacity style={styles.continueBtn} onPress={() => navigation.navigate('BookStoreDrawer')}>
        <Text style={styles.btnText}>CONTINUE</Text>
      </TouchableOpacity>
      </View>
      <TouchableOpacity style={styles.forgotbutton}  >
        <Text style={styles.btnfrgt}>FORGOT PASSWORD</Text>
      </TouchableOpacity>
      </ScrollView>
  </View>
    );
  }

  const styles = StyleSheet.create({
    container: {
      
         flex:1,
         backgroundColor: "white",
       },
    containercontinue: {
        marginTop:60,
         alignItems: "center",
          justifyContent: "center",  
        },

    containersignin: {
        marginTop:20,
        alignItems: "center",
        justifyContent: "space-around", 
        flexDirection:"row" 
        },
      
    black: {
        marginTop: 100,
        marginLeft:30,
        color: 'black',
        fontSize: 30,
        fontWeight: 'bold',
      },
    
    inputView: {
      marginTop: 50,
      marginLeft:10,
      backgroundColor: "white",
      borderBottomColor:'grey',
      borderBottomWidth:1,
      width: "95%",
      height: 50,
    },
    
    pwdView: {
      marginTop: 30,
      marginLeft:10,
      backgroundColor: "white",
      borderBottomColor:'grey',
      borderBottomWidth:1,
      width: "95%",
      height: 50,
      marginBottom: 20,
    },

    TextInput: {
      height: 50,
     fontSize:18,
      marginLeft: 10,
    },

    continueBtn: {
    //  marginLeft:10,
      width: screenWidth/1.05,
      borderRadius: 25,
      height: 50,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#6200EE",
    },

    signinBtn: {
        marginLeft:10,
        width: screenWidth/4.5,
        borderRadius: 30,
        height: 35,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#6200EE",
      },

    btnText:{
      color:"#fff",
      fontWeight: 'bold',
      fontSize:16
    },

    textsignin:{
        color:"#fff",
        fontWeight: 'bold',
        fontSize:14
      },
      textsignup:{
        color:"grey",
        fontWeight: 'bold',
        fontSize:14
      },

    forgotbutton: {
      marginTop: 40,
      height: 30,
      marginBottom: 30,
      alignItems: "center",
      justifyContent: "center",
    },

    btnfrgt:{
      color:"#6200EE",
      fontWeight: 'bold',
      
    },
  });

  export default Login;