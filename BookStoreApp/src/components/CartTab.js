import React ,{ useEffect, useState }from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator, Image, Button,TouchableOpacity, ImageBackground, Dimensions, ScrollView} from 'react-native';
import {fetchData} from '../service/api'
//import NewBooking from './src/components/NewBooking';
//import Iconloc from 'react-native-vector-icons/Entypo';
var screenHeight = Dimensions.get("screen").height;

export default CartTab = ({navigation}) => {

   return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button
        onPress={() => navigation.navigate('Home')}
        title="Cart"
      />
    </View>  
    );
   };
