import * as React from 'react';
import { Button, View,StatusBar,TouchableOpacity,Text,Image,StyleSheet,ImageBackground } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import Login from "./Login";
import HomeTab from "./HomeTab";
import BookDetailsTab from "./BookDetailsTab";
import CartTab from "./CartTab";
import OrdersTab from "./OrdersTab";
import Icon from 'react-native-vector-icons/Feather';
import CartIcon from 'react-native-vector-icons/AntDesign'; 
import SimpleIcon from 'react-native-vector-icons/Octicons';


// import {  
//     createSwitchNavigator,  
//     createAppContainer
// } from 'react-navigation';  

// const HomeTabStackNavigator = createStackNavigator(  
//     {  
//         HomeTabNavigator: HomeTab  
//     },  
//     {  
//         defaultNavigationOptions: ({ navigation }) => {  
//         return {  
//             headerLeft: (  
//                 <Icon  
//                     style={{ paddingLeft: 10 }}  
//                     onPress={() => navigation.openDrawer()}  
//                     name="md-menu"  
//                     size={30}  
//                 />  
//             )  
//         };  
//         }  
//     }  
// );  
  
// const BookDetailsTabStackNavigator = createStackNavigator(  
//     {  
//         BookDetailsTabNavigator: BookDetailsTab  
//     },  
//     {  
//         defaultNavigationOptions: ({ navigation }) => {  
//             return {  
//                 headerLeft: (  
//                     <Icon  
//                         style={{ paddingLeft: 10 }}  
//                         onPress={() => navigation.openDrawer()}  
//                         name="md-menu"  
//                         size={30}  
//                     />  
//                 )  
//             };  
//         }  
//     }  
// );  
// const AppDrawerNavigator = createDrawerNavigator({  
//     HomeTab: {  
//         screen: HomeTabStackNavigator  
//     },  
//     BookDetailsTab: {  
//         screen: BookDetailsTabStackNavigator  
//     },  
// });  
  
// const AppSwitchNavigator = createSwitchNavigator({  
//     HomeTab: { screen: AppDrawerNavigator },  
//     BookDetailsTab: { screen: BookDetailsTab },  
  
// });  
  
//const AppContainer = createAppContainer(AppSwitchNavigator);  
  

 const Drawer = createDrawerNavigator();

const BookStoreDrawer =({navigation}) =>{
   return (     
      <Drawer.Navigator 
     
      initialRouteName="Home"
     screenOptions={({ route }) => ({
      headerStyle: {
        backgroundColor: '#f4511e',
      },
        drawerIcon: ({ focused, color, size }) => {
          let iconName;
          
          if (route.name === 'Home') {
            return <Icon name="home" size={size} color={color} />;
          } 
          else if (route.name === 'Book Details') {
            return <SimpleIcon name="checklist" size={size} color={color} />;
          }
          else if (route.name === 'Cart') {
            return <CartIcon name="shoppingcart" size={size} color={color} />;
          }
          else if (route.name === 'Orders') {
            return <Icon name="shopping-bag" size={size} color={color} />;
          }
      
          // You can return any component that you like here!
          // return <Ionicons name={iconName} size={size} color={color} />;
        },
        
      })}
      
      drawerContentOptions={{
        activeTintColor: '#6200EE',
   //    inactiveTintColor: 'gray',
      }}
      
      >

        
        <Drawer.Screen name="Home" component={HomeTab} 
       
        />  
        <Drawer.Screen name="Book Details" component={BookDetailsTab} />
        <Drawer.Screen name="Cart" component={CartTab} />
        <Drawer.Screen name="Orders" component={OrdersTab} />
        <Drawer.Screen name="LOG OUT" component={Login} />
      
      </Drawer.Navigator>
       
      
  );
}

 const styles = StyleSheet.create({  
  icon:{
    borderRadius:360,
    marginLeft:30,
    width:100,
    height:100
  },
//     container: {  
//         flex: 1,  
//         alignItems: 'center',  
//         justifyContent: 'center'  
//     }  
 });  
export default BookStoreDrawer;