import React ,{ useEffect, useState }from 'react';
import { StyleSheet,useWindowDimensions, Text,Header,StatusBar, View, FlatList, ActivityIndicator, Image, Button,TouchableOpacity,LinearGradient, ImageBackground, Dimensions, ScrollView} from 'react-native';
import {fetchData} from '../service/api'
//import NewBooking from './src/components/NewBooking';
//import Iconloc from 'react-native-vector-icons/Entypo';
var screenHeight = Dimensions.get("screen").height;
import Icon from 'react-native-vector-icons/Feather';
//import { Header } from 'react-native/Libraries/NewAppScreen';

const numColumns = 2;

export default BookDetailsTab = ({navigation}) => {
  // navigationOptions = ({ navigation }) => ({
  //   headerStyle: {
  //     backgroundColor: "#13C0CE"
  //   },
   
  // });
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [orientation, setOrientation] = useState("")
  const window = useWindowDimensions()

  const getOrientation = () => {
    if (window.height < window.width) {
        setOrientation("LANDSCAPE")
    } else {
        setOrientation("PORTRAIT")
    }
    return orientation
}

  useEffect(() => {
    fetchData().then((result) => {
      // console.log("result",result)
      setData(result)
      setLoading(false)
      getOrientation()
    })
  }, [])



  
   return (
   
    <View style={styles.container}>
      <StatusBar backgroundColor='black' barStyle='dark-content' />
    
      <TouchableOpacity
        onPress={() => navigation.openDrawer()}>  
        {/* title="Home" */}
        <Icon name={'list'} size={20}/>
        </TouchableOpacity>

       
      
      {isLoading ? <ActivityIndicator /> : (
        <FlatList
          horizontal={false}
          showsVerticalScrollIndicator = {false}
          data={data}
          keyExtractor={item => item.id}
          renderItem={({ item }) => (
            <View flexDirection="row"> 
            <View style={styles.itemContainer}>
              {/* {console.log("Item",item)} */}
              <Image style={styles.item} source={{uri:item.cover}}/>
              </View>
              <Text style={{ justifyContent: "center", alignItems: "center",margin:10 ,fontSize:18}}>{item.title}</Text>
              {/* <View style={{ flexDirection: "row" }}>
                <Text style={styles.itemprice}>{item.Price}</Text>
                <View style={styles.itemstar}>
                <Text style={styles.itemstartext}>{item.rating}</Text>
                </View>
              </View> */}
            </View>
            
          )}
         
         
          />)}

    </View>  
    );
   };
  
   const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
       backgroundColor: "#fff"
    },
    itemContainer: {
      position: 'relative',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: 190,
      height: 190,
      // borderRadius: 10,
    //  backgroundColor: "#fff",
      marginBottom: 10,
    //  marginTop: "2%",
//  borderRadius: 15
    },
    item: {
      position: 'relative',
      width: 120,
      height: 170,
    },
    inputText: {
      // height: 50,
      color: "#454545",
      fontSize: 18,
    },
    SectionStyle: {
      flexDirection: 'row',
      // borderWidth: 1,
      borderColor: '#A9A9A9',
      height: 40,
      borderRadius: 20,
      margin: 5,
      width: "75%",
      backgroundColor: "#F2F2F2"
      // marginLeft: "5%"
    },
    ImageStyle: {
      padding: 10,
      margin: 10,
      height: 20,
      width: 20,
      resizeMode: 'stretch',
      alignItems: 'center'
    },
    itemstar: {
      height: 13,
      width: "15%",
      backgroundColor: "#ff6969",
      marginTop: "3%",
      justifyContent: "center",
      alignItems: "center",
      textAlign: "center"
    },
    itemprice: {
      marginRight: "10%",
      marginTop: "3%",
      fontWeight: "bold"
    },
    itemstartext: {
      color: "white",
      fontWeight: "900",
      fontSize: 10,
  
    }
  });
  