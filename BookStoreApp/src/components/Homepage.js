import React from 'react';
import { StyleSheet, Text, View,StatusBar, Image, Button,TouchableOpacity, ImageBackground, Dimensions, ScrollView} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
var screenHeight = Dimensions.get("screen").height;
var screenWidth = Dimensions.get("screen").width;

const Homepage = ({ navigation }) => {
    return (
      <View style={styles.container}>
         <StatusBar barStyle="dark-content" backgroundColor="#ecf0f1" />
        <ScrollView style={height="90%", backgroundColor="white"}>
        <View >
           <ImageBackground
            source={require('../assets/BookStore.jpg')}
            style={styles.backgroundImage}
            /> 
            <View style={styles.containertext}>
            <Text style={styles.blue}>Welcome</Text>  
            <Text style={styles.blue}>to Book Store</Text>
            </View>      
         </View>
         <View >
         <TouchableOpacity style={styles.signin} 
          onPress={() => navigation.navigate('Login')}>
            <Text style={styles.btnText}>SIGN IN
           </Text>     
        </TouchableOpacity>
        <TouchableOpacity style={styles.signup} 
          onPress={() => navigation.navigate('Login')}>
            <Text style={styles.btnText}>SIGN UP
           </Text>     
        </TouchableOpacity>
        </View>
        </ScrollView>
      </View>
    );
};
  
  const styles = StyleSheet.create({
    container: {
   //   marginTop: 30,
      backgroundColor: "white",
      flex:1,
      
    },
    containertext: {
     // paddingLeft:40,
     // paddingRight:30,
     marginTop:30,
      alignItems: "center",
       justifyContent: "center",  
       },
    blue: {
      color: '#6200EE',
      fontWeight: 'bold',
      fontSize: 40,
      alignItems: "center",
    justifyContent: "center",
    paddingLeft:50,
    paddingRight:50
    //  fontFamily:"Montserrat-Regular"
    },
    backgroundImage:{

      flex:1,
      resizeMode:"contain",
      height:screenHeight/2.5,
      width: '100%',
      justifyContent: "center",
    },
    signup: {
    paddingHorizontal: 30,
    paddingVertical: 20,
    backgroundColor: "#6200EE",
    alignSelf:"flex-end",
    width: screenWidth/1.5,
    textAlign: "left",
    height:screenHeight/7,
    alignItems: "center",
    justifyContent: "center",
    
    },

    signin: {
       marginTop:100,  
              
       paddingHorizontal: 30,
       paddingVertical: 20,
       backgroundColor: "#9599B3",
       alignSelf: "flex-end",
     //  marginHorizontal: "60%",
       width: screenWidth/1.5,
       height:screenHeight/10.5,
       textAlign: "left",
       alignItems: "center",
    justifyContent: "center",
       },
    alternativeLayoutButtonContainer: {
      margin: 20,
      flexDirection: 'row',
      justifyContent: 'space-between'
    },
    btnText:{
      color:"#fff",
      fontSize:17,
      fontWeight: 'bold',
    },
    icon:{
      width: 20,
    height: 20,
    
    },
  });
  
export default Homepage;
