/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
 import React from 'react';
 import { NavigationContainer } from '@react-navigation/native';
 import { createStackNavigator } from '@react-navigation/stack';
 import Homepage from './src/components/Homepage';
 import Login from './src/components/Login';
 import BookStoreDrawer from './src/components/BookStoreDrawer';
 import {StatusBar} from 'react-native';
import { color } from 'react-native-reanimated';
 
 const Stack = createStackNavigator();
 const App = () => {
      return (
         <NavigationContainer>
         <Stack.Navigator initialRouteName="Homepage" >
           <Stack.Screen name="Homepage" component = {Homepage}/>
            <Stack.Screen name="Login" component = {Login} />
          <Stack.Screen name="BookStoreDrawer" component = {BookStoreDrawer} 
         />
         </Stack.Navigator>
       </NavigationContainer>
       
      );
 }
 
 export default App;
 